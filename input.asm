;this file reads information from the console
section .data
	question db "what is your name?", 10 ;define byte 10 means new line
	message db "Hello, "
section .bss
	;declare variables that do not have initial values
	name resw 1
section .text 
	global _start
_start:


	call _ask
	call _getAnswer
	call _greeting
	call _printName
	;after calling procedures we return here and we exit.
	mov rax, 60
	mov rdi, 0
	syscall


_ask:
	mov rax, 1 ; id of sys_write
	mov rdi, 1 ; file descriptor argument 1
	mov rsi, question ; address for the data argumrnt 2
	mov rdx, 19  ; count of letters in the data argument 3
	syscall
	ret
_getAnswer:

	mov rax, 0 ; id of sys_read
	mov rdi, 0 ; also this one is zero
	mov rsi, name
	mov rdx, 16
	syscall
	ret
_greeting:

	mov rax, 1
	mov rdi, 1
	mov rsi, message
	mov rdx, 7
	syscall 
	ret

_printName:

	mov rax,1
	mov rdi,1
	mov rsi, name
	mov rdx, 16
	syscall
	ret 
