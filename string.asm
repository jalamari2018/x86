; print a string 
section .data
    text db "This is just a text written by Jebreel Alamari",10,0
section .bss

section .text 
	global _start
_start:
    mov rax, text ; get the localtion of text in rax
	call _print
	mov rax, 60
	mov rdi, 0
	syscall

; important note:
; it is possible to replace rbx by rdx then comment out line 35
_print:
    ;location of the text is in rax so we need to store it 
    push rax
    mov rbx, 0 ; set rbx to zero and use it as a counter for the loop
_loop:
    inc rax ; rax still has the location of the first char in the string 
    inc rbx  ; inc rbx becouse it is our counter
    mov cl, [rax] ; move one byte of location in rax to lower 8 bit of rcx
    cmp cl, 0 ; this sets some flag if cl is not 0 to 1 or to 0 if cl = 0
    jne _loop ; goes to _loop again if the cl value is not zero

    ;here else statement
    ;at this point rbx has length of the string but we need its location
    ;the starting location has been pushed to the stack so we can pop it 
    ; to the right register
    mov rax, 1
    mov rdi, 1
    pop rsi ; this takes the address of the string from the stack
    mov rdx, rbx ; this takes the legth of the string from rbx register
    syscall
    ret


; _printDigit:
;     add rax, 48 ; this increment whatever in rax by 48. 48 means zero in ascii table
; 	mov [digit], rax ; move the lower byte of rax to the digit location
;     ;now we can start printing 
;     mov rax, 1
;     mov rdi, 1
;     mov rsi, digit
;     mov rdx, 2 ; two bytes
;     syscall 
;     ret
