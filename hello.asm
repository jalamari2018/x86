section .data
	text db "Hello, World!", 10 ;define byte 10 means new line

section .text 
	global _start
_start:
	call _printHello
	mov rax, 60
	mov rdi, 0
	syscall


_printHello:
	mov rax, 1 ; id of sys_write
	mov rdi, 1 ; file descriptor argument 1
	mov rsi, text ; address for the data argumrnt 2
	mov rdx, 14  ; count of letters in the data argument 3
	syscall
	ret
	
