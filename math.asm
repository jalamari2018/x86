;this file contains math operations we can do with x86, mul and div assumes that one of the operands is rax register
;I think if we want to print a digit character, we need to use ascii table values

section .data
    ;digit db 0,10 ; this contains 2 bytes. zero and endline character
section .bss
    digit resq 1 ; this holds 64 bits, or 8 bytes the same length as rax
section .text 
	global _start
_start:
    mov rax, 4
	call _printDigit
	mov rax, 60
	mov rdi, 0
	syscall


_printDigit:
    add rax, 48 ; this increment whatever in rax by 48. 48 means zero in ascii table
	mov [digit], rax ; move the lower byte of rax to the digit location
    ;now we can start printing 
    mov rax, 1
    mov rdi, 1
    mov rsi, digit
    mov rdx, 2 ; two bytes
    syscall 
    ret
