; this is based on x86 book examples 

SIZE equ 1000 ; this a constant 

section .data
	name db "Jebreel"
	age  db 33
	grade db "a"
	
	salary dw 60000
	capital dd 4787800
	
section .bss
	message resb 10
	story resw 10
	longStory resd 100
	longerStory resq 100
	
	gpa resb 1 
section .text
	global _start
_start:

	;here we start the program 

	mov al,  [grade] ; mover a letter to the lower 8bit part of the rax register 
	mov [gpa], al ; moved the grade from al register to gpa variable in .bss section 


	; here we want the program to exit 
	mov rax, 60
	mov rdi, 0
	syscall    ; to cal syscall exit the application 

