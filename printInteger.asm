section .data
 
section .bss
    locationAddress resb 100; this address holds the string 
    index resb 8 ; this holds the index to a single character

section .text 
	global _start

_start:
    
    mov rax, 1254 ; this is the number we need to print to the console
    call _printInt
    call _exit

_printInt:
    
    mov rcx, locationAddress ; address of the string we want to make
    mov rbx, 10 ; new line character
    mov [rcx], rbx; add the character to the start of the string we need to build
    inc rcx ; incerement the position in the string we need to make
    mov [index], rcx ; move the new position to index variable 
_prepareStringLoop:
    ; this loop builds the the string of integers inside locationAddress space
    mov rdx, 0 ; prevent unexpected result because of this register 
    mov rbx, 10; divide by ten because are in decimal space 
    div rbx ; the result will be put inside rax. remainder in rdx by convention 
    push rax ; we save the result of our division to the stack
    add rdx, 48 ; adds 48 to the remainder in rdx, to get the correct char from the ascii table 
    
    mov rcx, [index]; this contains the address inside locationAddress string 
    mov [rcx], dl ; adding to the location in locationAddress
    inc rcx
    mov [index], rcx ; now we update the index to point to the next place in locationAddress string 

    pop rax ; get division result from the stack 
    cmp rax, 0 ; compare the result to zero. if it is zero that means we are done, we need to print the string in locationAdress because it contains the whole integer string 
    jne _prepareStringLoop ; we go back to the loop if rax is not = 0

_printLoop:
    ; this loop does the actual  printing 
    mov rcx, [index]; the value of this is the address to last character in the locationAddress string
    
    mov rax,1
    mov rdi, 1
    mov rsi, rcx ; the address of the digit character that we are goint to print
    mov rdx, 1
    syscall

    mov rcx, [index]; why redundent? it is possible, the value of rcx has been changed so we make sure that we have the right value in place
    dec rcx
    mov [index], rcx; decrement the index to the character in the string 

    cmp rcx, locationAddress; see if we have reached the start of the string or not 
    jge _printLoop 
    
    ret

 
_exit:
    ; this exit the program 
    mov rax, 60
	mov rdi, 0
	syscall